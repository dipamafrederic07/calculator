import sys
sys.path.insert(0, './src')

import unittest
from fonctions import *


class TestCalculator(unittest.TestCase):
    def test_addition(self):
        self.assertEqual(addition(7,5), 12)
        self.assertEqual(addition(85,0), 85)
        self.assertEqual(addition("fhfh","jj"), "Données invalide")
        self.assertEqual(addition(" ",2), "Données invalide")
    
    def test_soustraction(self):
        self.assertEqual(soustraction(7,5), 2)
        self.assertEqual(soustraction(5,"j"), "Données invalide")
        self.assertEqual(soustraction(-5," "), "Données invalide")
    
    def test_multiplication(self):
        self.assertEqual(multiplication(7,5), 35)
        self.assertEqual(multiplication(0,"jj"), "Données invalide")
        self.assertEqual(multiplication(100," "), "Données invalide")
    
    def test_division(self):
        self.assertEqual(division(7,5), 1.4)
        self.assertEqual(division(11,0), "Impossible de faire une division par 0")
        self.assertEqual(division(" ","jj"), "Données invalide")
        self.assertEqual(division(9,"jj"), "Données invalide")



    #Test pour les valeurs invalide
    #@unittest.expectedFailure
    #def test_echec(self):
        
        

        
        

        

        



if __name__ == '__main__':
    unittest.main()