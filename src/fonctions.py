#!/usr/bin/env python
#-*- coding: utf-8 -*-
def addition(a,b):
    try:
        return int(a)+int(b)
    except ValueError :
        return "Données invalide"

def  soustraction(a,b):
    try:
        return int(a)-int(b)
    except ValueError :
        return "Données invalide"

def multiplication(a,b):
    try:
        return int(a)*int(b)
    except ValueError :
        return "Données invalide"

def division(a,b):
    try:
        return float(a)/float(b)
    except ZeroDivisionError:
        return "Impossible de faire une division par 0"
    except ValueError :
        return "Données invalide"